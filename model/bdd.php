<?php

require("classes/typeactivite.class.php");

class BDD {
    
     public $conn;
    
    public function  __construct()
    {
        $this->conn= new PDO("mysql:host=localhost;dbname=forum_asso2", "root", "",array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    function getTypeActivite()
    {
        $stmt = $this->conn->prepare(" Select * from typeactivité");
        $stmt->execute();
  
        $resultat = ($stmt->fetchAll());
        var_dump($resultat);
        return $resultat;
    }

    function getTypeActivite2()
    {
        $stmt = $this->conn->prepare(" Select * from typeactivité");
        $stmt->execute();
  
        $resultat = ($stmt->fetchAll());
        $lesTypes=array();
        foreach($resultat as $unTypeActivite )
        {
            $ta=new TypeActivite($unTypeActivite['1']);
            $ta->NumTypeActivité=$unTypeActivite['0'];
            array_push($lesTypes,$ta);
            $lesActi=$this->getActiviteBy("NumTypeActivité",$unTypeActivite[0]);
            
        }
        var_dump($lesTypes);
        return $resultat;
    }

    function getActiviteBy($nomCol,$valeurCol)
    {
        $stmt = $this->conn->prepare(" Select * from activités where ".$nomCol."=:valeurCol");
        $stmt->bindParam(':valeurCol', $valeurCol);
        $stmt->execute(); 
        $resultat = ($stmt->fetchAll());
    //    var_dump($resultat);
       return $resultat;
        
    }

}

$testBdd=new BDD();
 $testBdd->getTypeActivite2();
// $testBdd->getActiviteBy("NumTypeActivité",1);
?>